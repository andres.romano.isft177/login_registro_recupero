<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
</head>
<body>
	<center>
		<form action="" method="post" >
			<font color="black" face="Courier New">
				<h2>Recupero de clave</h2>
				<br>
				<p>Ingresá el codigo que recibiste por mail y tu nueva contraseña</p>
				<table border="0" >
					<tbody>
						<tr>
							<td>Codigo:</td> <td><input type="text" name="code" required /></td>
						</tr>
						<tr>
							<td>Password:</td> <td><input type="password" name="pass" required /></td>
						</tr>				
						<tr><!--botones-->
							<td colspan="2"><center><input type="submit" name="confirmaPass" value="Confirmar"></center></td>	
						</tr>
					</tbody>
				</table>
			</font>
		</form>	
		<hr>
	</center>
</body>
</html>
<?php 
require_once ("funciones.php");

if (isset($_POST['confirmaPass'])) {

	$code = $_POST['code'];
	$pass = password_hash(trim($_POST['pass']), PASSWORD_DEFAULT);

	$actualizar = actualizarPass($archivo,$code,$pass);

	if ($actualizar == 1) {

		echo '<script>
          alert("Se actualizo la contraseña!!");
          window.location.href="index.php";
          </script>';

	} else {

		echo '<script>
          alert("El codigo ingresado no es correcto");          
          </script>';
          
	}

}