<?php 
session_start();
if (isset($_SESSION['email'])) :
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Incio</title>
</head>
<body>
	<h1>Bienvenido a la aplicacion</h1>
	<?php 
	echo "Usuario: ".$_SESSION['email']."<br>";
	echo "Nombre y Apellido: ".$_SESSION['nombre']." ".$_SESSION['apellido']."<br>";	
	?>
	<a href="salir.php">Salir</a>
</body>
</html>
<?php 
endif;
