<?php
# email|apellido|nombre|password ESTRUCTURA DEL ARCHIVO PLANO
$archivo = "usuarios.txt";

# FUNCIONES
# Verificar si el email ya esta registrado
function verificar($archivo,$email) {

	$ok = 0;// 0 si no lo encuentra

	if (is_readable($archivo)) {
		$file = fopen($archivo,"r");
		while (!feof($file)) {		
			$linea = fgets($file);												
			$datos = explode("|",$linea);
			if (strcmp(trim($email),trim($datos[0])) == 0) {				
				$ok = 1; // 1 si lo encontro					
				break;
			}//IF
		}//WHILE
		return $ok;
	} else {

		echo "no puedo abrir archivo";

	}

	fclose($archivo);
}

# Escribir en el archivo para registrar al usuario
function registrar($archivo,$email,$apellido,$nombre,$password) {

	$codigo = codigo();
	if (is_writable($archivo)) {
		$file = fopen($archivo,"a");
		fwrite($file, trim($email)."|".trim($apellido)."|".trim($nombre)."|".trim($password)."|".trim($codigo)."\n");
		echo '<script>
		alert("Registrado!");
		window.location.href="index.php";
		</script>';
	} else {
		echo "no puedo abrir archivo";
	}
	fclose($archivo);

}

# Autenticacion de usuario con email y contraseña
function login($archivo,$email,$password, $ok = 0) {
	
	if (is_readable($archivo)) {
		$file = fopen($archivo,"r");
		while (!feof($file)) {		
			$linea = fgets($file);					
			$datos = explode("|",$linea);
			if ( (strcmp(trim($email),trim($datos[0])) == 0) && (password_verify($password, $datos[3])) ) {				
				
				fclose($file);
				return $datos;

			} 
		}

		fclose($file);
		return $ok; 

	} else {

		echo '<script>
		alert("no puedo abrir archivo");
		window.location.href="inicio.php";
		</script>';
		
	}
	
} 

# Obtener el codigo de recuperacion con el mail
function obtenerCodigo($archivo,$email) {

	$ok = 0;// 0 si no lo encuentra

	if (is_readable($archivo)) {
		$file = fopen($archivo,"r");
		while (!feof($file)) {		
			$linea = fgets($file);												
			$datos = explode("|",$linea);
			if (strcmp(trim($email),trim($datos[0])) == 0) {				
				$ok = $datos;// si lo encontro, devuelvo todos los datos para armar el mail
				break;
			}//IF
		}//WHILE
		return $ok;
	} else {
		echo "no puedo abrir archivo";
	}
	fclose($archivo);

}

# generador de codigos
function codigo(){

	$cantidad = 10;	// cantidad de caracteres para el codigo	
	$caracteres = '0123456789abcdefghijklmnopqrstuvwxyz0123456789'; // combinacion de caracteres que se utilizaran
	$codigo = ''; // el codigo que sera retornado por la funcion
	$limite = strlen($caracteres);
	for ($i=0; $i < $cantidad; $i++){
		$codigo .= $caracteres[rand(0, $limite)];// el .= va a ir concatenando los caracteres
	}		
	return trim($codigo);

}

# crea el mensaje con los datos del usuario
function messageConcat($message,$name,$dato) {

//	reemplazo NAME y CODIGO
	$message = str_replace("NAME", $name, $message);
	$message = str_replace("CODIGO", $dato, $message);

//	devuelvo el mensaje personalizado
	return $message;

}

# Obtener el codigo de recuperacion con el mail
function actualizarPass($archivo,$code,$pass) {

	$bkp = "usuarios.txt.bak";
	touch(__DIR__."/".$bkp); // creo un nuevo archivo	
	chmod($bkp, 0777); // le doy permisos
	copy($archivo,$bkp); // copio los datos del original 
	$codigo = codigo(); // se le debe cambiar el codigo de recuperacion
	
	$ok = 0;// 0 si no lo encuentra
	if ( (is_readable($bkp)) && (is_writable($archivo)) ){
		$file = fopen($bkp,"r");
		$file2 = fopen($archivo,"w");
		while (!feof($file)) {		
			$linea = fgets($file);
			if ($linea === false) { // con esto excluyo las lineas vacias del archivo
				continue;
			} else {
				$datos = explode("|",$linea);
				if (strcmp(trim($code),trim($datos[4])) == 0) {	
			// cuando encuentra el codigo actualiza la contraseña y el nuevo codigo
					fwrite($file2, trim($datos[0])."|".trim($datos[1])."|".trim($datos[2])."|".trim($pass)."|".trim($codigo)."\n");
					$ok = 1;
				} else {
			// sino sigue grabando el archivo tal cual debe quedar
					fwrite($file2, trim($datos[0])."|".trim($datos[1])."|".trim($datos[2])."|".trim($datos[3])."|".trim($datos[4])."\n");
				}
			}				
		}//WHILE
		unlink($bkp); // borramos el archivo de bkp
		return $ok;
	} else {
		echo "no puedo abrir archivo";
	}
	fclose($archivo);

}
