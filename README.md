## Ejemplo practico

Ejemplo de login, registro y recupero de contraseña basico que
almacena los datos de usuario en un archivo. Tiene incluido
PHPMailer para notificar al usuario

### Variables de entorno

Aqui deben crear estas variables con los datos de la cuenta de 
email que van a utilizar, pueden sumarlas al archivo "docker-clase/.env"
que ya existe, pero deberan ejecutar nuevamente los contenedores con:
" sudo docker-compose up -d --build " 

```
MAIL_SERVER=smtp.gmail.com
MAIL_USER=mail@gmail.com
MAIL_PASS=clave
```

> NOTA: deben clonarlo en la carpeta public

Prof. Andrés D. Romano