<!DOCTYPE html>
<html>
<head>
	<title>Registro</title>
	<meta charset="utf-8"/> 
</head>
<body>		
	<CENTER>
		<form action="" method="post">
			<font color="black" face="Courier New">				
				<h2>Ingrese datos de registro</h2>
				<br>
				<table border="0">
					<tbody>
						<tr>
							<td>Email</td> <td><input type="email" name="email" placeholder="nombre@ejemplo.com" required />*</td>
						</tr>
						<tr>
							<td>Apellido</td> <td><input type="text" name="apellido" placeholder="ingrese su apellido" required />*</td>
						</tr>
						<tr>
							<td>Nombre</td> <td><input type="text" name="nombre" placeholder="ingrese su nombre" required />*</td>
						</tr>			
						<tr>
							<td>Password</td> <td><input type="password" name="password" maxlength="10" required >*</td>
						</tr>
						<tr>
							<td colspan="2">
								<p>Los campos con * son obligatorios</p>
							</td>
						</tr>			
						<tr><!--botones-->
							<td colspan="2"><center><input type="submit" name="registro" value="Registrarse"></center></td>
						</tr>
					</tbody>
				</table>
			</font>
		</form>	
		<hr>
		<a href="index.php">Login</a>
		<a href="recupero.php">Recuperar clave</a>
	</CENTER>
</body>
</html>
<?php
require_once ("funciones.php");

if(isset($_POST['registro'])) {

	$email = $_POST['email'];
	$apellido = $_POST['apellido'];
	$nombre = $_POST['nombre'];
	$password = password_hash(trim($_POST['password']), PASSWORD_DEFAULT);

	$ok = verificar($archivo,$email);//la funcion devuelve 1 si encontro el mail
	
	if($ok == 0) { // pregunto si OK es 0, osea si no lo encontro	

		registrar($archivo,$email,$apellido,$nombre,$password);//lo registro		

	} else {

		echo '<script>
          alert("El email ya esta registrado");          
          </script>';

	}	

}
