<?php
# incluyo mi archivo de funciones
require_once ("funciones.php");

# Se debe verificar que los archivos que se requieren a continuación se encuentren en las rutas indicadas  
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require_once './PHPMailer/Exception.php';
require_once './PHPMailer/PHPMailer.php';
require_once './PHPMailer/SMTP.php';

if (isset($_POST['recupero'])) {    

    $email = trim($_POST['email']);
    $validar = obtenerCodigo($archivo,$email);
    if ($validar != 0) {
        $name = $validar[1]." ".$validar[2];
        $codigo = $validar[4];
        $message = file_get_contents("mailRecupero.html");
        $message = messageConcat($message,$name,$codigo);
    //  Se instancia un objeto de la clase PHPMailer

        $mail = new PHPMailer(true);

        try {
    //  Configuración del servidor
            $mail->SMTPDebug = SMTP::DEBUG_SERVER;  
            $mail->isSMTP(); 
            $mail->Host = $_ENV['MAIL_SERVER'];
            $mail->SMTPAuth = true;     //Se habilita la autenticación smtp
            $mail->Username = $_ENV['MAIL_USER']; //Colocar aquí una dirección de correo valida
            $mail->Password = $_ENV['MAIL_PASS']; //Colocar aquí la contraseña
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         
            $mail->Port = 587;  //Número del puerto utilizado
            $mail->setFrom($_ENV['MAIL_USER'], 'Recupero de clave');
            $mail->addAddress($email);     //A quién se le envía el mail, el nombre es opcional

    //  Contenido
            $mail->Subject = 'Envio de consulta';  //Asunto del mensaje
            $mail->isHTML(true);
            $mail->msgHTML($message); // se envia en formato HTML
            
            if ($mail->send()) {

                echo '<script>
                alert("Se envio un codigo por mail para validar tu recuperacion de cuenta");
                window.location.href="password.php"
                </script>';

            }
    //  Fin del try
        } catch (Exception $e) {

            echo "Error, el mensaje no se envió: {$mail->ErrorInfo}"; //Si hay algún error
            
        }
        
    } else {
    //  sino es valido el mail informo y redirijo para registrarse
        echo '<script>
        alert("No tenemos ese mail registrado");
        window.location.href="registro.php"
        </script>';
    }
}
