<?php 
session_start();
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
</head>
<body>
	<center>
		<form action="" method="post" >
			<font color="black" face="Courier New">
				<h2>Ingrese al Sitio</h2>
				<br>
				<table border="0" >
					<tbody>
						<tr>
							<td>Email:</td> <td><input type="email" name="email" placeholder="nombre@ejemplo.com" required /></td>
						</tr>
						<tr>
							<td>Password:</td> <td><input type="password" name="password" required ></td>
						</tr>				
						<tr><!--botones-->
							<td colspan="2"><center><input type="submit" name="login" value="Ingresar"></center></td>
						</tr>
					</tbody>
				</table>
			</font>
		</form>	
		<hr>
		<a href="registro.php">Registro</a>
		<a href="recupero.php">Recuperar clave</a>
	</center>
</body>
</html>
<?php
require_once ("funciones.php");

if (isset($_POST['login'])) {

	$email = trim($_POST['email']);
	$password = trim($_POST['password']);		

	$ok = login($archivo,$email,$password);

	if (is_array($ok)) {

		$_SESSION['email'] = $ok[0];
		$_SESSION['apellido'] = $ok[1];
		$_SESSION['nombre'] = $ok[2];
		$_SESSION['codigo'] = $ok[4];
		echo '<script>
          alert("Bienvenido!!");
          window.location.href="inicio.php";
          </script>';

	} else {

		echo '<script>
          alert("Usuario ó Contraseña incorrectos");
          </script>';

	}
	
}	

