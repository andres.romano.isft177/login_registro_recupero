<?php 
session_start();

if (isset($_SESSION['email'])) {

	session_destroy();
	unset($_SESSION['email']);
	unset($_SESSION['apellido']);
	unset($_SESSION['nombre']);
	unset($_SESSION['codigo']);

}

header("Location:index.php");
